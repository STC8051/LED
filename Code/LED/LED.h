#ifndef __LED_H
#define __LED_H

void LED_Init(void);
void LED_On(unsigned char n);
void LED_Off(unsigned char n);
void LED_Toggle(unsigned char n);

#endif