#include <STC12C5A60S2.h>

#define LED	P0

void LED_Init(void)
{
	LED = 0xFF;
}

void LED_On(unsigned char n)
{
	LED &= ~(1 << n);
}

void LED_Off(unsigned char n)
{
	LED |= (1 << n);
}

void LED_Toggle(unsigned char n)
{
	unsigned char sta = 0;
	sta = ((LED >> n) & 0x01);
	
	if (sta) {
		LED &= ~(1 << n);
	} else {
		LED |= (1 << n);
	}
}
